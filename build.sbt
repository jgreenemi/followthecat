name := "FollowTheCat"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % "10.0.9",
  "org.slf4j" % "slf4j-api" % "1.7.5",
  "org.slf4j" % "slf4j-simple" % "1.7.5",
  "com.gu" %% "scanamo" % "0.9.5",
  "com.amazonaws" % "aws-java-sdk-dynamodb" % "1.11.171"
)