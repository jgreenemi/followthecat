## Follow.The.Cat

A browser-based RPG, serving as a learning tool for Akka HTTP and DynamoDB.

### Dependencies

- [**Akka HTTP**](http://doc.akka.io/docs/akka-http/current/scala/http/): Web request routing!
- [**Scanamo**](https://guardian.github.io/scanamo/): A library for simplifying interactions with our AWS DynamoDB datastore.
- [**SLF4J**](https://www.slf4j.org/): Simple Logging Facade for Java, for logging purposes.