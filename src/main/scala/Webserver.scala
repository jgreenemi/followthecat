import java.io.File

import akka.actor.ActorSystem
import akka.http.scaladsl.{Http, server}
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import followthecat.Toolkit
import org.slf4j.LoggerFactory

import scala.io.StdIn

/**
  * Create an Akka HTTP frontend.
  */
object Webserver {
  val logger = LoggerFactory.getLogger("Webserver")

  def main(args: Array[String]): Unit = {
    logger.info("Starting Webserver.main().")

    implicit val system = ActorSystem("webserver-system")
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val htmlContent: Map[String,server.Route] = Toolkit.htmlLoader

    val routes =
      pathPrefix("static") {
        getFromDirectory("src/main/resources/static")
      } ~
      path("status") {
        get {
          htmlContent("status.html")
        }
      } ~
      path("pwd") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, s"${new File(".").getAbsolutePath}"))
        }
      } ~
      path("") {
        get {
          htmlContent("index.html")
        }
      }

    /**
      * Set webserver bindings.
      */
    val bindingFuture = Http().bindAndHandle(routes, "localhost", 8080)

    logger.info(s"Server online at http://localhost:8080/ Press Return to stop.")
    StdIn.readLine()
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }
}
