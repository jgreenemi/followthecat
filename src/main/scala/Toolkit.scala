package followthecat

import akka.http.scaladsl.server
import akka.http.scaladsl.server.Directives._
import java.io.File
import org.slf4j.LoggerFactory

object Toolkit {
  val logger = LoggerFactory.getLogger("Toolkit")

  def htmlLoader(): Map[String,server.Route] = {
    /**
      * For all the HTML files in the resources/static/html directory, read them in
      * as strings to a dictionary.
      */
    logger.info("Starting Toolkit.htmlLoader().")

    val path = "src/main/resources/static/html/"
    val resources_static_html = new File(path)
    val htmlFiles: List[File] = resources_static_html.listFiles.filter(_.isFile).toList
    var htmlContents: Map[String,server.Route] = Map()

    for(filepath <- htmlFiles) {
      htmlContents += (
        s"${filepath.getName}" -> getFromFile(s"$filepath")
      )
    }

    // DEBUG
    for((k, v) <- htmlContents) {
      logger.info(s"Loaded $k!")
    }

    return htmlContents
  }
}
