package followthecat

import com.gu.scanamo._
import com.gu.scanamo.error._
import com.gu.scanamo.syntax._
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType._

object DBInteractor {
  case class FTCCharacter(name: String)

  def createCharacter(name: String): Option[Either[DynamoReadError,FTCCharacter]] = {
    val dbclient = LocalDynamoDB.client() // Not sure why LocalDynamoDB is not being recognized in Intellij?
    val characterTableResult = LocalDynamoDB.createTable(dbclient)("character")('name -> S)

    val putResult = Scanamo.put(dbclient)("character")(FTCCharacter(s"$name"))
    println(s"$putResult")

    val getResult = Scanamo.get[FTCCharacter](dbclient)("character")('name -> s"$name")
    return getResult
  }

  def main(args: Array[String]): Unit = {
    println(createCharacter("Snorkmaiden"))
  }
}
